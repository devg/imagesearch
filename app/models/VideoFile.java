package models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
@Table(name = "video_file")
public class VideoFile extends Model {
	@Required
    public String name;
    public String fileName;

    public String url;
    
    @OneToMany(mappedBy="video")
    public Set<ImageFile> keyFrames;
}