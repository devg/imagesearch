package models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
@Table(name = "image_file")
public class ImageFile extends Model {
	@Required
    public String name;
   
	public Integer minutes; // minutes into video
	public Integer seconds; // seconds into video
	public Integer frameNumber; // frame number in video
	
    @ManyToOne
    public VideoFile video;
}