package controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import models.ImageFile;
import models.VideoFile;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.imageterrier.basictools.BasicSearcher;

import play.Logger;
import play.mvc.Controller;
import utils.ImageUtils;
import utils.SearchResult;
import flexjson.JSONSerializer;

public class Process extends Controller {
	
	public static void index() {
		renderTemplate("index.html");
	}
	
	public static void search() {
		renderTemplate("search.html");
	}
	
	
	public static void indexImage(File[] files) {
		File[] newFiles = new File[files.length];
		int num = 0;
		File imgDir = new File("data/");
		if (!imgDir.exists()) imgDir.mkdirs();
		File outDir = new File("data/imaajes");
		if (!outDir.exists()) outDir.mkdirs();
		File indexDir = new File("data/index");
		if (!indexDir.exists()) indexDir.mkdirs();
		for (File f:files)
		{
			Logger.info("Index File:" + f.getName());
			ImageFile img = new ImageFile();
			img.name = f.getName();
			img.save();
			File permFile = new File(outDir, ""+img.id + f.getName().substring(f.getName().lastIndexOf(".")));
			try {
				Logger.info("Index File:" + f);
				// copyFile(f, new File(outDir, ""+img.id)); // Does not work
				InputStream in = new FileInputStream(f);
				OutputStream out = new FileOutputStream(permFile);
				
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0)
				{
				        out.write(buf, 0, len);
				}
				in.close();
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}
			newFiles[num++] = permFile;
		}
		//BasicIndexer -o munch.idx -q quantiserFile -qt RANDOMSET -k 1000 -p BYTE -ft ASIFT  imagesdir -v
		// --mode (-m) [IMAGES | QUANTISED_FEATUR : input mode
		// ES]                                    :
		//  --output (-o) path                     : path at which to write index
		//  --type (-t) [BASIC | NEAREST_NEIGHBOUR : Choose index type
		//  | POSITION | AFFINESIM]                :
		//  --verbose (-v)                         : print verbose output
//		String[] args = new String[10];
//		args[0] = "-o";
//		args[1] = indexDir.getAbsolutePath();
//		args[2] = "-m";
//		args[3] = "IMAGES";
//		args[4] = "-t";
//		args[5] = "BASIC";
//		args[6] = "-v";
//		args[7] = "-k";
//		args[8] = "1000";
//		args[9] = imgDir.getAbsolutePath();
		ImageUtils.indexFiles(outDir.getAbsolutePath(), indexDir.getAbsolutePath());
//		for (File f:newFiles)
//		{
//			if (f == null) continue;
//			f.renameTo(new File(imgDir, f.getName()));
//		}
//		File[] delFiles = outDir.listFiles();
//		for (File f:delFiles)
//		{
//			f.delete();
//		}
		String message = "" + num + " Files indexed";
		String agent = request.headers.get("user-agent").toString();
		Logger.info("Agent:" + agent);
		String type = params.get("type");
		if ("mobile".equals(type))
		{
	        renderJSON(new JSONSerializer().exclude("class").serialize(message));
			return;
		}
		renderTemplate("index.html",message);
	}
	
	public static void indexVideos(File[] files) {
		File vidDir = new File("public/videos");
		if (!vidDir.exists()) vidDir.mkdirs();
		File outDir = new File("data/imaajes");
		if (!outDir.exists()) outDir.mkdirs();
		File frameDir = new File("data/frame" + Thread.currentThread().getId()); // Need to make unique to thread
		if (!frameDir.exists()) frameDir.mkdirs();
		File indexDir = new File("data/index");
		if (!indexDir.exists()) indexDir.mkdirs();
		VideoFile[] videoFiles = null;
		if (files == null || files.length == 0)
		{
			String videoURL = params.get("videoURL");
			Logger.info("Video URL:" + videoURL);
			if (videoURL == null)
			{
				String message = "Invalid Input";
				String type = params.get("type");
				if ("mobile".equals(type))
				{
			        renderJSON(new JSONSerializer().exclude("class").serialize(message));
					return;
				}
				renderTemplate("index.html",message);
				return;
			}
			VideoFile video = new VideoFile();
			if (videoURL.lastIndexOf("/") != -1 && videoURL.length() > videoURL.lastIndexOf("/")-1)
				video.name = videoURL.substring(videoURL.lastIndexOf("/")+1);
			else
				video.name = "";
			video.url = videoURL;
			video.save();
			File permFile = new File(vidDir, ""+video.id + video.name);
			CloseableHttpClient httpclient = null;
			try {
				if(!permFile.exists())
				{
					permFile.createNewFile();
				}
				FileOutputStream fileOutputStream = new FileOutputStream(permFile);
				httpclient = HttpClients.custom().build();
			    String userCredentials = "username:password";
			    HttpGet httpget = new HttpGet(videoURL);
			   // httpget.setHeader("Content-Type", "text/html");
			    CloseableHttpResponse response = httpclient.execute(httpget);
		        try {
		            fileOutputStream.write(EntityUtils.toByteArray(response.getEntity()));
		            fileOutputStream.close();
		        } finally {
		            response.close();
		        }
		    } catch (Exception x) {
		        x.printStackTrace();
		    }finally {
		        try {httpclient.close();} catch (Exception e) {}
		    }
			files = new File[1];
			files[0] = permFile;
			video.fileName = permFile.getName();
			video.save();
			videoFiles = new VideoFile[1];
			videoFiles[0] = video;
		}
		else
		{
			Logger.info("Index Video:" + files[0].getName());
			videoFiles = new VideoFile[files.length];
			int i = 0;
			for (File file: files)
			{			
				VideoFile video = new VideoFile();
				video.name = file.getName();
				video.save();
				video.fileName = ""+video.id + file.getName();
				//video.url = videoURL;
				video.save();
				videoFiles[i++] = video;
				file.renameTo(new File(vidDir, video.fileName));
			}
		}
		int j = 0;
		int imgcnt = 0;
		for (VideoFile video: videoFiles)
		{
			File permFile = new File(vidDir, video.fileName);
			try {
				File[] frameFiles = frameDir.listFiles();
				int i = 0;
				for (File frameFile: frameFiles)
				{
					frameFile.delete();
				}
				ImageUtils.extractKeyFrames(permFile, frameDir);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			File[] frameFiles = frameDir.listFiles();
			if (frameFiles.length == 0)
			{
				String message = "Error extracting key frames";
				video.delete();
				if (videoFiles.length == 1)
				{
					String type = params.get("type");
					if ("mobile".equals(type))
					{
				        renderJSON(new JSONSerializer().exclude("class").serialize(message));
						return;
					}
					renderTemplate("index.html",message);
					return;
				}
				else
					continue;
			}
			int i = 0;
			if (video.keyFrames == null) video.keyFrames = new HashSet<ImageFile>();
			for (File f:frameFiles)
			{
				if (f == null) continue;
				ImageFile frame = new ImageFile();
				frame.name = f.getName();
				frame.video = videoFiles[j];
				frame.frameNumber = i++;
				frame.save();
				frame.name = ""+frame.id + f.getName().substring(f.getName().lastIndexOf("."));
				frame.save();
				video.keyFrames.add(frame);
				f.renameTo(new File(frameDir, frame.name));
				try {
					ImageUtils.copyFile(new File(frameDir, frame.name), new File(outDir, frame.name));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					continue;
				}
			}
			frameFiles = frameDir.listFiles();
			ImageUtils.indexFiles(outDir.getAbsolutePath(), indexDir.getAbsolutePath());
			for (File f:frameFiles)
			{
				if (f == null) continue;
				Logger.info("Frame:"+  f.getName());
				f.renameTo(new File(vidDir, f.getName()));
			}
			//permFile.renameTo(new File(vidDir, permFile.getName()));
			video.save();
			imgcnt = imgcnt + frameFiles.length;
			File[] delFiles = frameDir.listFiles();
			for (File f:delFiles)
			{
				f.delete();
			}
			j++;
		}
		String message = "" + files.length + " Videos " + imgcnt + " Images indexed";
		String agent = request.headers.get("user-agent").toString();
		Logger.info("Agent:" + agent);
		String type = params.get("type");
		if ("mobile".equals(type))
		{
	        renderJSON(new JSONSerializer().exclude("class").serialize(message));
			return;
		}
		renderTemplate("index.html",message);
		frameDir.delete();
	}
	
	public static void searchImage(File image) {
		Logger.info("Search:" + image.getName());
		File imgDir = new File("data/imaajes");
		File indexDir = new File("data/index");
		if (!indexDir.exists()) indexDir.mkdirs();
		File vidDir = new File("public/videos");
		//BasicSearcher -i munch.idx -q .\query.jpeg
		String[] args = new String[4];
		args[0] = "-i";
		args[1] = indexDir.getAbsolutePath();
		args[2] = "-q";
		args[3] = image.getAbsolutePath();
		String output ="";
		List<SearchResult> results = new ArrayList<SearchResult>();
		try {
			PrintStream prevOut = System.out;
			OutputStream byteOut = new ByteArrayOutputStream(1000);
			PrintStream pOut = new PrintStream(byteOut);
			System.setOut(pOut);
			BasicSearcher.main(args);
			System.setOut(prevOut);
			output = byteOut.toString();
			System.out.println("Output:" + output);
			String[] lines = output.split("\\n");
			Set<Long> uniqResults = new HashSet<Long>();
			for (String line: lines)
			{
				if (line.indexOf("imaajes") == -1) continue;
				String[] filescore = line.split("\\t");
				String fileId = null;
				String fileName = null;
				String score = null;
				for (String token: filescore)
				{
					if (token.trim().length() == 0) continue;
					if (token.indexOf("imaajes") != -1)
					{
						fileName = token.substring(token.indexOf("imaajes") + 8);
						 if (fileName.startsWith("/") || fileName.startsWith("\\"))
							 fileName = fileName.substring(1);
						
						if (fileName.indexOf(".") != -1)
							fileId = fileName.substring(0, fileName.indexOf("."));
						 if (fileId.startsWith("/") || fileId.startsWith("\\"))
							 fileId = fileId.substring(1);
						 if (fileName.indexOf(".fv") != -1)
							 fileName = fileName.substring(0, fileName.indexOf(".fv"));
					}
					else
						score = token;
				}
				if (fileId != null)
				{
					ImageFile img = ImageFile.findById(new Long(fileId));
					if (img != null)
					{
						SearchResult result = new SearchResult();
						result.name = img.name;
						result.score = score;
						result.fileName = fileName;
						if (img.video != null)
						{
							if (uniqResults.contains(img.video.id)) continue;
							uniqResults.add(img.video.id);
							result.videoFileName = img.video.name;
							result.videoId = img.video.id;
							result.videoURL = img.video.url;
							result.minutesIntoVideo = img.minutes;
							result.secondsIntoVideo = img.seconds;
						}
						results.add(result);
					}
					else 
						Logger.error("fileId:" + fileId + " is null");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			output = e.toString();
		}
		for (SearchResult result:results)
		{
			result.queryFile = image.getName();
			File src = new File(imgDir, result.fileName);
			if (!src.exists())
				 src = new File(vidDir, result.name);
			if (!src.exists())
				 continue;
			File dest = new File("public/result/" + result.fileName);
			File resdir = new File("public/result");
			if (!resdir.exists()) resdir.mkdirs();
			try {
				//copyFile(src, dest); // Does not work
				InputStream in = new FileInputStream(src);
				OutputStream out = new FileOutputStream(dest);
				
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0)
				{
				        out.write(buf, 0, len);
				}
				in.close();
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			ImageUtils.copyFile(image, new File("public/result/" + image.getName()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String type = params.get("type");
		if (results.size() > 0)
		{
			if ("mobile".equals(type))
			{
		        renderJSON(new JSONSerializer().exclude("class").serialize(results));
				return;
			}
			renderTemplate("index.html", results);
		}
		else
		{
			String message2 = "No Match Found";
			if ("mobile".equals(type))
			{
		        renderJSON(new JSONSerializer().serialize(message2));
				return;
			}
			renderTemplate("index.html", message2);
		}
			
	}
	
	public static void fakeSearch(File image) {
		List<SearchResult> results = new ArrayList<SearchResult>();
		SearchResult result = new SearchResult();
		result.name = "frame2.jpg";
		result.score = "30.00";
		result.fileName = "1000.jpg";
		result.videoFileName = "football.avi";
		result.videoURL = "http://video.repository.com/football.avi";
		result.minutesIntoVideo = 5;
		result.secondsIntoVideo = 20;
		results.add(result);
		if (results.size() > 0)
		{
	        renderJSON(new JSONSerializer().exclude("class").serialize(results));
			return;
		}
		else
		{
			String message = "No Match Found";
	        renderJSON(new JSONSerializer().serialize(message));
			return;
		}
			
	}

}
