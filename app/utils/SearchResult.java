package utils;

public class SearchResult {
	public String queryFile;
	public String name;
	public String score;
	public String fileName;
	public String videoFileName;
	public Long videoId;
	public String videoURL;
	public Integer minutesIntoVideo;
	public Integer secondsIntoVideo;

}
