package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.imageterrier.basictools.BasicIndexer;

import play.Logger;
import play.Play;

public class ImageUtils {
	
	public static void indexFiles(String imageDirectory, String indexDirectory)
	{
		String[] args = new String[14];
		args[0] = "-o";
		args[1] = indexDirectory;
		args[2] = "-q";
		args[3] = "quantiserFile";
		args[4] = "-qt";
		args[5] = "RANDOMSET";
		args[6] = "-p";
		args[7] = "BYTE";
		args[8] = "-ft";
		args[9] = "ASIFT";
		args[10] = "-v";
		args[11] = "-k";
		args[12] = "1000";
		args[13] = imageDirectory;
		try {
			BasicIndexer.main(args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void indexFiles3_5(String imageDirectory, String indexDirectory)
	{
		String[] args = new String[14];
		args[0] = "-o";
		args[1] = indexDirectory;
		args[2] = "-q";
		args[3] = "quantiserFile";
		args[4] = "-qt";
		args[5] = "RANDOMSET";
		args[6] = "-p";
		args[7] = "BYTE";
		args[8] = "-ft";
		args[9] = "ASIFT";
		args[10] = "-v";
		args[11] = "-k";
		args[12] = "1000";
		args[13] = imageDirectory;
		try {
			BasicIndexer.main(args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
     
    public static void extractKeyFrames(File videoFile, File frameDir) throws Exception
    {
		Logger.info("extractKeyFrames:" + videoFile + ":" + videoFile.exists() + ":" + frameDir + ":" + frameDir.exists());
    	// ffmpeg -i test.mp4 -vf select='eq(pict_type\,I)',setpts='N/(25*TB)' ./keyFrame_files/testTemp\%09d.jpg
		String command = Play.configuration.getProperty("ffmpeg.command","/usr/bin/ffmpeg -i __VIDEO_FILE__ -vf select='eq(pict_type\\,I)',setpts='N/(25*TB)' __IMAGE_DIR__\\%09d.jpg");
		Logger.info("command:" + command);
		command = command.replace("__VIDEO_FILE__", videoFile.getAbsolutePath().replace(" ", "%20")).replace("__IMAGE_DIR__", frameDir.getAbsolutePath() + File.separator);
		Logger.info("command:" + command);
	
//		Runtime runtime = Runtime.getRuntime();
//		java.lang.Process process = runtime.exec(command);
//		InputStream input = process.getInputStream();
//        StringBuffer buffer = new StringBuffer();
//        int daLine = -1;
//        while((daLine = input.read()) != -1)
//        {
//            buffer.append(daLine);
//            if (daLine == '\n' || daLine == '\r')
//            	Logger.info(buffer.toString());
//        }
//        input.close();
//        if (buffer.indexOf("Error") != -1)
//            Logger.error(buffer.toString());
//        else
//        	process.waitFor();
		String[] params = command.split(" ");
        ProcessBuilder pb = new ProcessBuilder(params);
        pb.redirectErrorStream(true);
        Process process = pb.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null)
            System.out.println("tasklist: " + line);
        process.waitFor();        
        return;
    }
	
    public static void copyFile(File src, File dst) throws IOException
    {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);
		
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0)
		{
		        out.write(buf, 0, len);
		}
		in.close();
		out.close();
    }

}
